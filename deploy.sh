#!/bin/bash

if [ $(id -u) -eq 0 ]
then
	echo "Run this script with user-rights!"
	exit
fi

if [ -d "$1" ]
then
	export VALHEIM_WWWDIR="$1"
else
	echo "First parameter is the www-path (i. e. '/var/www/html/my-server-1234567')"
	exit
fi


export myuser="$USER"
export USER_WWW_PATH="$HOME"/public_html

mkdir $USER_WWW_PATH && chmod ug+rwx $USER_WWW_PATH
#chown www-data:www-data ~/public_html #needs super-user rights
mkdir ~/bin
mkdir ~/pids
cp valheim.sh ~/bin/
mkdir --parents "$VALHEIM_WWWDIR"
cp *.php *.html "$VALHEIM_WWWDIR"

sudo sh -c "mkdir --parents $VALHEIM_WWWDIR && chown $myuser:www-data $VALHEIM_WWWDIR && chown $USER:www-data $USER_WWW_PATH"

echo 'Consider adding lines similar to the following two lines to your /etc/sudoers.conf file using "visudo"'
echo 'Defaults	secure_path="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/snap/bin:/home/$USER/bin/"'
echo 'www-data ALL = ($USER) NOPASSWD: /home/$USER/bin/valheim.sh'
echo 'Make sure you understand what you do using "man sudoers.conf" since this is a security-issue.'
echo "To enable ~/public_html, make your HOME-dir accessable for user or group 'www-data'."
