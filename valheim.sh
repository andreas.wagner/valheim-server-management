#!/bin/bash

export PIDFILE=$HOME"/pids/valheim.pid"
export WORLDS_DATA=$HOME"/.config/unity3d/IronGate/Valheim/worlds_local/"
export VALHEIMPATH=$HOME"/Steam/steamapps/common/Valheim dedicated server"
export NOHUPFILE="$VALHEIMPATH"/nohup.out

case $1 in
start)
	export templdpath="$LD_LIBRARY_PATH"
	export LD_LIBRARY_PATH=./linux64:"$LD_LIBRARY_PATH"
	if [ -f "$PIDFILE" ]
	then
		echo "Valheim already running. (PIDfile exists.)"
		exit;
	fi
	cd "$VALHEIMPATH"
	rm nohup.out
	echo "Starting server; Execute »$0 stop« to exit."
	nohup ./valheim_server.x86_64 -name "Andys World" -port 2456 -nographics -batchmode -world "CloseSwamps" -password "12345" -public 0 > nohup.out &
	echo $! > "$PIDFILE"
	export LD_LIBRARY_PATH="$templdpath"
;;

stop)
	if [ ! -f "$PIDFILE" ]
	then
		echo "Valhein not running. (PIDFile does not exist.)"
		exit;
	fi
	kill $(cat "$PIDFILE")
	rm "$PIDFILE"
;;

pid)
	if [ ! -f "$PIDFILE" ]
	then
		echo -n "not running"
	else
		if [ $(ps --pid=$(cat "$PIDFILE")|wc -l) = 2 ]
		then
			echo -n $(cat "$PIDFILE")
		else
			echo -n "Probably crashed!"
		fi
	fi
;;

status)
	if test -e "$NOHUPFILE"
	then
		cat "$NOHUPFILE"
	else
		echo "$NOHUPFILE does not exist"
	fi
;;

backup)
	echo zip   "$WORLDS_DATA"
	if test -e "$WORLDS_DATA"
	then
		nohup zip -r ~/public_html/"$(date -Iseconds)".zip "$WORLDS_DATA/" > nohup.out &
		echo "test"
	else
		echo "WORLDS_DATA not a directory"
	fi
;;

update)
	echo "updating"
	cd ~/Steam/steamapps/common/'Valheim dedicated server'
	rm nohup.out
	nohup steamcmd +login anonymous +app_update 896660 validate +exit > nohup.out &
;;

*)
	echo '"pid", "status", "backup", "update", "start" or "stop" supported!'
;;
esac
